# 3 Or 5 Rockstar

An [Rockstar](https://codewithrockstar.com/) solution to [Euler Project #1](https://projecteuler.net/problem=1).


> If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.
> Find the sum of all the multiples of 3 or 5 below 1000.

This solution is made to use a little bit of an obscure programming language, Gitlab pipelines, and Gitpod configuration to make it super easy. 
Hit the Open in Gitpod button and watch it run.

[![Open in Gitpod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#https://gitlab.com/mjh/3-or-5-rockstar)

## CI pipelines

This uses Gitlab pipelines to build the Rockstar libraries once before running yarn tests and running the code itself. 
This makes it so it only has to build the interpreter once and once those artifacts are available it can do the `test` and `production` steps simultaneously, leveraging [DAG](https://docs.gitlab.com/ee/ci/directed_acyclic_graph/) and then `needs:` keyword.

## Gitpod

This uses Gitpod to prebuild the [Rockstar Interpreter](https://github.com/RockstarLang/rockstar/tree/main/satriani) so that you can simply run `node rockstar/satriani/rockstar three-or-five.rock` to get your results (again, it runs it once for you). 
It also means you don't need to have node installed and definitely don't need to have this interpreter clogging up your machine for a small example.

## Why

Mostly wanted to show off how an obscure language can use Gitpod to get rolling and Gitlab to get testing to minimize the "extra" stuff that you have to normally setup. By click the button above you can run and see this code (and probably come up with better lyrics) and if you fork it, you get tests and deployment off a single build.

Also, now I am a [Rockstar Developer](https://github.com/RockstarLang/rockstar#but-why). A quick fork and you can be too!

![Rockstar Def Leppard Wallpaper](https://codewithrockstar.com/media/wallpaper/def_leppard_hd_wallpaper.png)